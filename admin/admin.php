<?php

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Call the admin setup on init.
add_action( 'init', '_bfrf_admin_setup' );


/**
 * Sets up the admin.
 */
function _bfrf_admin_setup() {

	add_action( 'admin_init', '_bfrf_admin_register_settings' );
	add_action( 'admin_menu', '_bfrf_admin_menu' );
	add_filter( 'plugin_action_links', '_bfrf_admin_add_settings_link', 10, 2 );
	add_action( 'admin_enqueue_scripts', '_bfrf_admin_enqueue_scripts' );
}

/**
 * Add the settings sections and individual settings.
 */
function _bfrf_admin_register_settings() {
	global $bf_registration_form;

	add_settings_section( 'bfrf_admin_config_settings', __( 'Admin Config Settings', 'bf_registration_form' ), '_bfrf_settings_callback_config_admin', 'bf-registration-form' );

 	// Country
 	add_settings_field( '_bfrf_country', '<label for="_bfrf_country">' . __( 'Country', 'bf_registration_form' ) . '</label>', 'bfrf_country_field', 'bf-registration-form', 'bfrf_admin_config_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_country', 'esc_attr' );

 	// Brand Id Drop Down
 	add_settings_field( '_bfrf_brand_id_dropdown', '<label for="_bfrf_brand_id_dropdown">' . __( 'Brand Id *', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_bfrf_brand_id_dropdown', 'bf-registration-form', 'bfrf_admin_config_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_brand_id_dropdown', 'esc_attr' );

 	// Production URL
 	add_settings_field( '_bfrf_prod_url', '<label for="_bfrf_prod_url">' . __( 'Production URL', 'bf_registration_form' ) . '&nbsp<a href="#" title="Fully qualified (http://www.yoursite.com), no trailing slash">?</a></label>', 'bfrf_prod_url_field', 'bf-registration-form', 'bfrf_admin_config_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_prod_url', 'esc_attr' );

	//==========================================================================================================================================================================================//

 	// Segment Id
 	add_settings_field( '_bfrf_segment_id', '<label for="_bfrf_segment_id">' . __( 'Segment Id', 'bf_registration_form' ) . '</label>', 'bfrf_segment_id_field', 'bf-registration-form', 'bfrf_admin_config_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_segment_id', 'esc_attr' );

 	// Source code
 	add_settings_field( '_bfrf_source_code', '<label for="_bfrf_source_code">' . __( 'Source Code', 'bf_registration_form' ) . '</label>', 'bfrf_source_code_field', 'bf-registration-form', 'bfrf_admin_config_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_source_code', 'esc_attr' );

	//==========================================================================================================================================================================================//

	add_settings_section( 'bfrf_admin_g_recaptcha_settings', __( 'Google Recaptcha Settings', 'bf_registration_form' ), '_bfrf_settings_callback_g_recaptcha', 'bf-registration-form' );

	//Disable/Enable G Recaptcha
 	add_settings_field( '_bfrf_g_recaptcha_disable','<label for="_bfrf_g_recaptcha_disable">' . __( 'Google Recaptcha', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_g_recaptcha_disable', 'bf-registration-form', 'bfrf_admin_g_recaptcha_settings');
 	register_setting  ( 'bf-registration-form', '_bfrf_g_recaptcha_disable', 'esc_attr' );

	// Site key
 	add_settings_field( '_bfrf_g_site_key', '<label for="_bfrf_g_site_key">' . __( 'Site Key', 'bf_registration_form' ) . '&nbsp;&nbsp;<a href="https://developers.google.com/recaptcha/docs/start" target="_blank" title="NoCaptcha Getting Started Guide">?</a></label>', 'bfrf_g_site_key_field', 'bf-registration-form', 'bfrf_admin_g_recaptcha_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_g_site_key', 'esc_attr' );

	// Secret key
 	add_settings_field( '_bfrf_g_secret_key', '<label for="_bfrf_g_secret_key">' . __( 'Secret Key', 'bf_registration_form' ) . '</label>', 'bfrf_g_secret_key_field', 'bf-registration-form', 'bfrf_admin_g_recaptcha_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_g_secret_key', 'esc_attr' );

	//==========================================================================================================================================================================================//

 	// User settings
 	add_settings_section( 'bfrf_user_settings', __( 'User Notification Settings', 'bf_registration_form' ), '_bfrf_settings_callback_user', 'bf-registration-form' );

	//Disable user e-mail
 	add_settings_field( '_bfrf_user_email_disable','<label for="_bfrf_user_email_disable">' . __( 'User e-mail follow-up', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_user_email_disable', 'bf-registration-form', 'bfrf_user_settings');
 	register_setting  ( 'bf-registration-form', '_bfrf_user_email_disable', 'esc_attr' );

 	// User E-mail From
 	add_settings_field( '_bfrf_user_email_from_setting', '<label for="_bfrf_user_email_from_setting">' . __( 'User Email From', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_user_email_from', 'bf-registration-form', 'bfrf_user_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_user_email_from_setting', 'esc_attr' );

 	// User subject
 	add_settings_field( '_bfrf_user_subject_setting', '<label for="_bfrf_user_subject_setting">' . __( 'User Email Subject', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_user_subject', 'bf-registration-form', 'bfrf_user_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_user_subject_setting', 'esc_attr' );

 	// User message
 	add_settings_field( '_bfrf_user_message_setting', '<label for="_bfrf_user_message_setting">' . __( 'User Email Body', 'bf_registration_form' ) . '</label>', '_bfrf_settings_callback_user_message', 'bf-registration-form', 'bfrf_user_settings' );
 	register_setting  ( 'bf-registration-form', '_bfrf_user_message_setting', 'esc_attr' );

	do_action( '_bfrf_admin_register_settings' );

}

/**
 * Add to the settings menu.
 */
function _bfrf_admin_menu() {

	add_options_page ( __( 'B-F Registration Form',  'bf_registration_form' ), __( 'B-F Registration Form',  'bf_registration_form' ), 'manage_options', 'bf-registration-form', '_bfrf_settings_page' );
}

/**
 * Add a direct link to the settings page from the plugins page.
 */
function _bfrf_admin_add_settings_link( $links, $file ) {

	global $bf_registration_form;

	if ( $bf_registration_form->basename == $file ) {
		$settings_link = '<a href="' . add_query_arg( 'page', 'bf-registration-form', admin_url( 'options-general.php' ) ) . '">' . __( 'Settings', 'bf_registration_form' ) . '</a>';
		array_unshift( $links, $settings_link );
	}

	return $links;
}

/**
 * Enqueue the scripts.
 */
function _bfrf_admin_enqueue_scripts( $page ) {
	// Add custom css or javascript
	wp_register_script( 'bfrf-admin', plugins_url('public/js/admin.js', dirname(__FILE__) ), array('jquery'), NULL, true );
	wp_enqueue_script('bfrf-admin');
}