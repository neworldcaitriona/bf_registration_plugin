<?php

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Define the settings page.
 */
function _bfrf_settings_page() { ?>

	<div class="wrap">

		<?php screen_icon(); ?>

		<h2><?php esc_html_e( 'B-F Registration Form Settings', 'bf_registration_form' ) ?></h2>

		<form action="options.php" method="post">

			<?php settings_fields( 'bf-registration-form' ); ?>

			<?php do_settings_sections( 'bf-registration-form' ); ?>

			<?php submit_button(); ?>

		</form>
	</div>

<?php }

/**
 * Prints the admin settings section heading.
 */
function _bfrf_settings_callback_config_admin() {

	echo '<p>' . esc_html__( 'These settings are for the basic configuration of the plugin.', 'bf_registration_form' ) . '</p>';
}

/**
 * Prints the admin create consumer settings section heading.
 */
function _bfrf_settings_callback_config_create_consumer() {

	echo '<p>' . esc_html__( 'These settings are for the create consumer feature of the plugin.', 'bf_registration_form' ) . '</p>';
}

/**
 * Prints the Google Recaptcha settings section heading.
 */
function _bfrf_settings_callback_g_recaptcha() {

	echo '<p>' . esc_html__( 'These settings are for the Google Recaptcha configuration of the plugin.', 'bf_registration_form' ) . '</p>';

}


/**
 * Prints the admin settings section heading.
 */
function _bfrf_settings_callback_admin() {

	echo '<p>' . esc_html__( 'These settings are for the email sent to the admin on a new user signup.', 'bf_registration_form' ) . '</p>';

}

/**
 * Prints the site key settings field.
 */
function bfrf_g_site_key_field() { ?>

	<input name="_bfrf_g_site_key" type="text" id="_bfrf_g_site_key" value="<?php echo esc_attr( get_option( '_bfrf_g_site_key', __( '', 'bf_registration_form' ) ) ); ?>" class="regular-text" />

<?php }

/**
 * Prints the secret key settings field.
 */
function bfrf_g_secret_key_field() { ?>

	<input name="_bfrf_g_secret_key" type="text" id="_bfrf_g_secret_key" value="<?php echo esc_attr( get_option( '_bfrf_g_secret_key', __( '', 'bf_registration_form' ) ) ); ?>" class="regular-text" />

<?php }

/**
 * Prints the preference country settings field.
 */
function bfrf_country_field() { ?>

	<select name="_bfrf_country" id="_bfrf_country" data-default-iso="<?php echo get_option( '_bfrf_country', 'us' ); ?>"></select>

<?php }

function bfrf_get_brands_json(){
	ob_start();
	include('brands.json');
	$b = ob_get_contents();
	ob_end_clean();
	return json_decode($b);
}

/**
 * Prints the brand id drop down field.
 */
function _bfrf_settings_callback_bfrf_brand_id_dropdown() {
	$name = '_bfrf_brand_id_dropdown';
	$selected = get_option( $name, '' );
	$selected = explode('|', $selected)[0];

	$brands = bfrf_get_brands_json()->brands;

	?>

	<select name="<?php echo $name; ?>" id="<?php echo $name; ?>" class="regular-text" required>
		<option value="" <?php echo $selected == '' ? 'selected':'';?>>(Choose Brand)</option>
		<?php
			foreach($brands as $brand=>$v){
				$val = $v->sf_id.'|'.$brand.'|'.$v->brand_id;
				$s = $selected == $v->sf_id ? ' selected':'';
				printf('<option value="%s"%s>%s</option>', $val, $s, $brand);
			}
		?>
	</select>

	<?php
};

/**
 * Prints the segment id settings field.
 */
function bfrf_segment_id_field() { ?>

	<input name="_bfrf_segment_id" type="text" id="_bfrf_segment_id" value="<?php echo esc_attr( get_option( '_bfrf_segment_id', __( '4', 'bf_registration_form' ) ) ); ?>" class="small-text" />

<?php }

/**
 * Prints the source code settings field.
 */
function bfrf_source_code_field() { ?>

	<input name="_bfrf_source_code" type="text" id="_bfrf_source_code" value="<?php echo esc_attr( get_option( '_bfrf_source_code', __( '', 'bf_registration_form' ) ) ); ?>" class="small-text" placeholder="0000" />

<?php }

/**
 * Admin email field.
 */
function _bfrf_settings_callback_admin_email() { ?>

	<input name="_admin_email_setting" type="text" id="_admin_email_setting" value="<?php echo esc_attr( get_option( '_admin_email_setting', __( '', 'bf_registration_form' ) ) ); ?>" placeholder="<?php _e('Email address that receives notification on new user signup.', 'bf_registration_form'); ?>" class="regular-text" />
<?php }

/**
 * Disable Admin E-mail notification field.
 */
function _bfrf_settings_callback_admin_email_disable() {
	$name = '_admin_email_disable';
	$selected = get_option( $name, 'enabled' );
	?>

	<select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
		<option value="disabled" <?php echo $selected == 'disabled' ? 'selected':'';?>>Disabled</option>
		<option value="enabled" <?php echo $selected == 'enabled' ? 'selected':'';?>>Enabled</option>
	</select>

	<?php
};

/**
 * Admin subject field.
 */
function _bfrf_settings_callback_admin_subject() { ?>

	<input name="_admin_subject_setting" type="text" id="_admin_subject_setting" value="<?php echo esc_attr( get_option( '_admin_subject_setting', __( '', 'bf_registration_form' ) ) ); ?>" placeholder="<?php _e('Subject Line for the email sent to the admin user(s).', 'bf_registration_form'); ?>" class="regular-text" />
<?php }

/**
 * Admin message field.
 */
function _bfrf_settings_callback_admin_message() { ?>

	<textarea name="_admin_message_setting" type="text" id="_admin_message_setting" cols="100" rows="6" placeholder="<?php _e('New user registration on the site.', 'bf_registration_form'); ?>" class="regular-text" ><?php echo esc_attr( get_option( '_admin_message_setting', __( '', 'bf_registration_form' ) ) ); ?></textarea>
<?php }

/**
 * Disable User E-mail notification field.
 */
function _bfrf_settings_callback_g_recaptcha_disable() {
	$name = '_bfrf_g_recaptcha_disable';
	$selected = get_option( $name, 'disabled' );
	?>

	<select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
		<option value="disabled" <?php echo $selected == 'disabled' ? 'selected':'';?>>Disabled</option>
		<option value="enabled" <?php echo $selected == 'enabled' ? 'selected':'';?>>Enabled</option>
	</select>

	<?php
}

/**
 * Prints the user settings section heading.
 */
function _bfrf_settings_callback_user() {

	echo '<p>' . esc_html__( 'These settings are for the email sent to the new user on their signup.', 'bf_registration_form' ) . '</p>';
}

/**
 * Disable User E-mail notification field.
 */
function _bfrf_settings_callback_user_email_disable() {
	$name = '_bfrf_user_email_disable';
	$selected = get_option( $name, 'disabled' );
	?>

	<select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
		<option value="disabled" <?php echo $selected == 'disabled' ? 'selected':'';?>>Disabled</option>
		<option value="enabled" <?php echo $selected == 'enabled' ? 'selected':'';?>>Enabled</option>
	</select>

	<?php
};

/**
 * User subject field.
 */
function _bfrf_settings_callback_user_email_from() { ?>

	<input name="_bfrf_user_email_from_setting" type="text" id="_bfrf_user_email_from_setting" value="<?php echo esc_attr( get_option( '_bfrf_user_email_from_setting', __( '', 'bf_registration_form' ) ) ); ?>" placeholder="<?php _e('E-mail Address to show as From in user e-mail.', 'bf_registration_form'); ?>" class="regular-text" />
<?php }

/**
 * User subject field.
 */
function _bfrf_settings_callback_user_subject() { ?>

	<input name="_bfrf_user_subject_setting" type="text" id="_bfrf_user_subject_setting" value="<?php echo esc_attr( get_option( '_bfrf_user_subject_setting', __( '', 'bf_registration_form' ) ) ); ?>" placeholder="<?php _e('Subject line for the welcome email sent to the user.', 'bf_registration_form'); ?>" class="regular-text" />
<?php }

/**
 * User message field.
 */
function _bfrf_settings_callback_user_message() { ?>

	<textarea name="_bfrf_user_message_setting" type="text" id="_bfrf_user_message_setting" cols="100" rows="6" placeholder="<?php _e('Your registration details.', 'bf_registration_form'); ?>" class="regular-text" ><?php echo esc_attr( get_option( '_bfrf_user_message_setting', __( '', 'bf_registration_form' ) ) ); ?></textarea>
<?php }

/**
 * Prints the production URL settings field.
 */

function bfrf_prod_url_field() { ?>

	<input name="_bfrf_prod_url" type="text" id="_bfrf_prod_url" value="<?php echo esc_attr( get_option( '_bfrf_prod_url', __( '', 'bf_registration_form' ) ) ); ?>" class="regular-text" placeholder="Fully qualified (http://www.yoursite.com), no trailing slash" />

<?php }

?>