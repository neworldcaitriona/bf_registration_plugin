<?php
    //PROMOTION RESPONSE QUESTIONS AREA
    /*
    $promotion_questions = array(
        array(
            'question'=>'Question #1',
            'type' => 'text',
            'required' => true,
            'placeholder' => 'Answer me'
        ),
        array(
            'question'=>'Question #2',
            'type' => 'textarea'
        ),
        array(
            'question'=>'Question #3',
            'type'=>'select',
            'required' => true,
            'options'=>array(
                'Option 1' => 1,
                '--select plz --'=>"",
                'Option 2' => '2'
            ),
            'selected' => 2
        ),
        array(
            'question'=>'Question #4',
            'type'=>'select',
            'required' => true,
            'options'=>array(
                'Option 1',
                'Option 2'
            )
        ),
        array(
            'question'=>'Question #5',
            'type'=>'radio',
            'required' => true,
            'options'=>array(
                'Option 1' => 1,
                'Option 2' => '2'
            )
        ),
        array(
            'question'=>'Question #6',
            'type' => 'checkbox',
            'checked_value' => 'yes',
            'not_checked_value' => 'no'
        )
    );
    */

    if (!empty($_GET["success"]) && strtolower($_GET["success"]) == "yes"): ?>
        <div class="bfrf-form-outer">
            <div class="bfrf-form-inner">
                <div class="bfrf-form-container bfrf-thank-you">
                    <p>Thank you for your entry. We will respond to you as soon as possible.</p>
                    <a href="/">Return to homepage</a>
                </div>
            </div>
        </div>

        <?php return;
    endif;

/*******************************
// IF CREATE CONSUMER SETTING IS ENABLED:
// Use these fields to add inputs to the form
// Then add them to the form_data variable in the "bfrf_form_submit" function in app.js

*   Required Fields
*   - bday
*   - bmonth
*   - byear
*   - source_code
*   - brand_id
*   - zip_postal
*   - country
*
*   Optional
*   - segment_id //Default to 4 if not supplied
*   - first_name
*   - last_name
*   - middle_initial
*   - gender
*   - address_1
*   - address_2
*   - city
*   - state_prov_terr
*   - us_state_abbrv
*   - email
*   - home_phone
*   - mobile_phone
*   - send_email
*   - send_mail
*   - send_sms

if send_sms = 'Y'
    mobile_phone required

if send_email = 'Y'
    email required
********************************/

    if (empty(bfrf_brand_id_dropdown())) {
        return;
    }

    $required = 'required="required" aria-required="true"';
    $brand = explode('|', bfrf_brand_id_dropdown());

?>
<div class="bfrf-form-outer">
    <div class="bfrf-form-inner">
        <div class="bfrf-form-container">
            <!--  ----------------------------------------------------------------------  -->
            <!--  NOTE: Please add the following <FORM> element to your page.             -->
            <!--  ----------------------------------------------------------------------  -->
            <form method="POST" class="bfrf-form" id="bfrf_form">
                <input type="hidden" name="send_user_email" id="send_user_email" value="<?php echo bfrf_user_email_disable_setting()=='enabled' ? 'true' : 'false'; ?>" >
                <input type="hidden" name="permalink" id="permalink" value="<?php echo get_permalink(); ?>" >

                <input type="hidden" id="brand_id" name="brand_id" value="<?php echo $brand[2]; ?>">
                <input type="hidden" id="segment_id" name="segment_id" value="4">
                <input type="hidden" id="source_code" name="source_code" value="<?php echo bfrf_source_code_setting(); ?>">

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="first_name">First Name <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><input id="first_name" maxlength="80" name="first_name" size="20" type="text" <?php echo $required; ?>></div>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="last_name">Last Name <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><input id="last_name" maxlength="80" name="last_name" size="20" type="text" <?php echo $required; ?>></div>
                </div>

                <a id="bf-error" name="bf-error"></a>
                <div class="bfrf-field-container bf-error-container">
                    <span class="lda-error bf-error">You must be of legal drinking age to enter.</span>
                    <span class="date-error bf-error">Invalid date entered.</span>
                </div>

                <div class="bfrf-field-container">
                    <fieldset id="fieldset-lda">
                        <div class="bfrf-field-label">
                            <legend>Birth Date <span class="bf-star">*</span></legend>
                        </div>
                        <div class="bfrf-field-input bfrf-field-input-lda">
                            <div class="bfrf-lda-fields" id="lda-fields">
                                <div class="bfrf-lda-field"><label for="bmonth" class="sr-only">Month</label><input type="number" id="bmonth" name="bmonth" min="01" max="12" placeholder="MM" <?php echo $required; ?>></div>
                                <div class="bfrf-lda-field"><label for="bday" class="sr-only">Day</label><input type="number" id="bday" name="bday" min="01" max="31" placeholder="DD" <?php echo $required; ?>></div>
                                <div class="bfrf-lda-field"><label for="byear" class="sr-only">Year</label><input type="number" id="byear" name="byear" min="1900" max="<?php echo date('Y'); ?>"  placeholder="YYYY" <?php echo $required; ?>></div>
                            </div>
                        </div>
                    </fieldset>
                </div>


                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="address_1">Street Address</label></div>
                    <div class="bfrf-field-input"><input name="address_1" id="address_1" type="text" maxlength="255" size="20"></div>
                </div>


                <!--
                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="city">City</label></div>
                    <div class="bfrf-field-input"><input name="city" id="city" type="text" maxlength="255" size="20"></div>
                </div>
                -->

                <div class="bfrf-field-container">
                    <?php //If state is required field, mark all select and inputs as required like normal, add star to labels ?>
                    <fieldset id="fieldset-us_state_abbrv" class="state" data-iso="us">
                        <div class="bfrf-field-label"><label for="us_state_abbrv">State <span class="bf-star">*</span></label></div>
                        <div class="bfrf-field-input">
                            <select name="us_state_abbrv" id="us_state_abbrv" <?php echo $required; ?>></select>
                        </div>
                    </fieldset>

                    <fieldset id="fieldset-state_prov_terr" class="state" data-iso="au">
                        <div class="bfrf-field-label"><label for="state_prov_terr">Province <span class="bf-star">*</span></label></div>
                        <div class="bfrf-field-input">
                            <select name="state_prov_terr" id="state_prov_terr" <?php echo $required; ?>></select>
                        </div>
                    </fieldset>

                    <fieldset id="fieldset-state_prov_terr" class="state" data-iso="none">
                        <div class="bfrf-field-label"><label for="state_prov_terr">Province <span class="bf-star">*</span></label></div>
                        <div class="bfrf-field-input">
                            <input type="text" id="state_prov_terr" name="state_prov_terr" <?php echo $required; ?>>
                        </div>
                    </fieldset>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="zip_postal">Zip Code <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><input name="zip_postal" id="zip_postal" type="text" maxlength="255" size="20" <?php echo $required; ?>></div>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="country">Country <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><select id="country" name="country" data-default-iso="<?php echo bfrf_country_setting();?>" <?php echo $required; ?>></select></div>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="email">Email <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><input id="email" maxlength="80" name="email" size="20" type="email" <?php echo $required; ?>></div>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="confirm_email">Confirm Email <span class="bf-star">*</span></label></div>
                    <div class="bfrf-field-input"><input id="confirm_email" maxlength="80" name="confirm_email" size="20" type="email" <?php echo $required; ?>></div>
                </div>

                <!--  Additional Field Possibilities
                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="mobile_phone">Mobile Phone Number</label></div>
                    <div class="bfrf-field-input"><input id="mobile_phone" maxlength="80" name="mobile_phone" size="20" type="tel"></div>
                </div>

                <div class="bfrf-field-container">
                    <div class="bfrf-field-label"><label for="gender">Gender</label></div>
                    <div class="bfrf-field-input">
                        <select id="gender" name="gender">
                            <option value="">Gender</option>
                            <option value="F">Female</option>
                            <option value="M">Male</option>
                            <option value="Z">Other</option>
                            <option value="">Prefer not to say</option>
                        </select>
                    </div>
                </div>
                -->

                <?php
                    if (!empty($promotion_questions)) {
                        bfrf_print_promotion_responses($promotion_questions);
                    }
                ?>

                <div class="bfrf-field-container">
                    I expressly give my consent to the processing of my personal data for promotional activities by Brown-Forman Corporation, other companies of the Brown-Forman group and data processors for Brown-Forman as detailed in the <a href="/privacy/" target="_blank">Privacy Policy</a>.
                </div>

                <div class="bfrf-field-container">
                    <label for="send_email"><input name="send_email" type="checkbox" id="send_email" value="Y">&nbsp;&nbsp;I would like to receive e-newsletters.</label>
                </div>

                 <?php if (bfrf_g_recaptcha_disable_setting() == "enabled" && !empty(bfrf_g_site_key_setting()) && !empty(bfrf_g_secret_key_setting())): ?>
                    <div class="bfrf-field-container">
                        <div id="g_recaptcha">
                            <div class="g-recaptcha" data-sitekey="<?php echo bfrf_g_site_key_setting(); ?>" data-callback="bfrf_g_recaptcha_success" data-expired-callback="bfrf_g_recaptcha_expired"></div>
                        </div>
                    </div>
                    <div class="bfrf-submit">
                        <input type="submit" id="registration-submit" value="Submit" disabled>
                    </div>
                <?php else: ?>
                    <div class="bfrf-submit">
                        <input type="submit" id="registration-submit" value="Submit">
                    </div>
                <?php endif; ?>

            </form>
        </div>
    </div>
</div>

<div class="bfrf-lightbox" id="bfrf-thank-you-lightbox">
    <div class="bfrf-lightbox--close-container">
        <i class="fa fa-times-circle-o" aria-hidden="true" id="bfrf_lightbox_close_button"></i>
    </div>
    <div class="bfrf-lightbox--message" id="bfrf_lightbox_message"></div>
    <div class="bfrf-lightbox-button">
        <a href="/">Return to homepage</a>
    </div>
</div>
<div class="bfrf-lightbox" id="bfrf-loading-lightbox">
    <div class="bfrf-lightbox--message" id="bfrf_lightbox_message"></div>
</div>
<div class="bfrf-lightbox" id="bfrf-error-lightbox">
    <div class="bfrf-lightbox--close-container">
        <i class="fa fa-times-circle-o" aria-hidden="true" id="bfrf_lightbox_close_button"></i>
    </div>
    <div class="bfrf-lightbox--message" id="bfrf_lightbox_message"></div>
    <div class="bfrf-lightbox-button">
        <a href="/">Return to homepage</a>
    </div>
</div>
<?php

?>