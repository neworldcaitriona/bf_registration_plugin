<?php
/*
 * Example: [bf-registration-form]
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/*
 * Shortcode Registration Form

 */
add_shortcode('bf-registration-form', 'bfrf_registration_shortcode_func');

add_action('wp_ajax_send_user_email', 'bfrf_send_user_email');
add_action('wp_ajax_send_user_email', 'bfrf_send_user_email');

function bfrf_send_user_email( ) {
	if (bfrf_user_email_disable_setting()==="enabled") {
		$from = bfrf_user_email_from_setting();
		$subject = bfrf_user_subject_setting();
		$message = bfrf_user_message_setting();

		if (!empty($from) && !empty($subject) && !empty($message) ) {

			try {

				$to = sanitize_email($_POST['email']);

				$headers = "From: NoReply <".$from.">\n";
				$headers.= "Content-Type: text/plain; charset=UTF-8\n";
				$headers.= "Content-Transfer-Encoding: 8bit\n";

				$res = wp_mail($to, $subject, $message, $headers);

				if ($res) {
					echo "success";
					wp_die();
					return;
				} else {
					http_response_code(400);
					echo "fail";
					wp_die();
					return;
				}

			} catch(Exception $e) {
				http_response_code(400);
				echo "fail";
				wp_die();
				return;
			}

		}

		http_response_code(400);
		echo "missing information in config";
		wp_die();
		return;

	}

	echo "send_user_email not enabled";
	wp_die();
	return;

}

function bfrf_sanitizer($str) {
	return sanitize_text_field(trim($str));
}

function bfrf_g_recaptcha($g_recaptcha) {
	$data = array(
	    'secret' => bfrf_g_secret_key_setting(),
	    'response' => bfrf_sanitizer($g_recaptcha)
	);
	$verify = curl_init();
	curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($verify, CURLOPT_POST, true);
	curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($verify);

	return $response;
	wp_die();
	//how to use
	//$google = json_decode($this->bfrf_g_recaptcha($request->get('g-recaptcha-response')));
}

function bfrf_g_recaptcha_check() {
	if (empty($_POST['g-recaptcha-response'])) {
		echo "g-recaptcha-required";
		return false;
	} else {
		$google = json_decode(bfrf_g_recaptcha($_POST['g-recaptcha-response']));
		return $google->success;
	}
	wp_die();
}

function bfrf_get_value_email($key) {
	if (!empty($_POST[$key])) {
		return sanitize_email($_POST[$key]);
	}
	return "";
}

function bfrf_get_value($key) {
	if (!empty($_POST[$key])) {
		return sanitize_text_field($_POST[$key]);
	}
	return "";
}

//function bf_force_web_to_case() {
		//if (bfrf_g_recaptcha_disable_setting() == "enabled" && !empty(bfrf_g_site_key_setting()) && !empty(bfrf_g_secret_key_setting())) {
		//	if (empty($_POST['g-recaptcha-response'])) {
		//		throw new Exception('g-recaptcha-required');
		//	} else if (!bfrf_g_recaptcha_check()) {
		//		throw new Exception('g-recaptcha-failed');
		//	}
		//}

		//$brand = explode('|', bfrf_brand_id_dropdown());
		//throw error?
		//http_response_code(400);
//}

function bfrf_registration_shortcode_func( $atts ) {
	$privacy_policy_link=__( 'privacy', 'bf_registration_form' );

	ob_start();
	@include_once('bfrf_form.php');
	$form = ob_get_contents();
	ob_end_clean();

	bfrf_client_scripts();

	return $form;
}

function bfrf_client_scripts() {
	//registered in bf_registration_plugin.php
	wp_localize_script( 'bfrf-app', 'ajax_url', admin_url('admin-ajax.php') );
	wp_enqueue_script('bfrf-app');

	if (bfrf_g_recaptcha_disable_setting()=="enabled") {
		wp_enqueue_script('bfrf-ncrc', 'https://www.google.com/recaptcha/api.js');
	}

	wp_enqueue_style('bfrf-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
}

/**
 * Retrieves user email enable/disable from db.
 */
function bfrf_admin_email_disable_setting() {
	return apply_filters( 'bfrf_admin_email_disable', get_option( '_admin_email_disable', 'enabled' ) );
}

/**
 * Retrieves recaptcha enable/disable from db.
 */
function bfrf_g_recaptcha_disable_setting() {
	return apply_filters( 'bfrf_g_recaptcha_disable', get_option( '_g_recaptcha_disable', 'disabled' ) );
}

/**
 * Retrieves recaptcha site key from db.

 */
function bfrf_g_site_key_setting() {
	return apply_filters( 'bfrf_g_recaptcha_site_key', get_option( '_bfrf_g_site_key', '' ) );
}

/**
 * Retrieves admin email from db.
 */
function bfrf_admin_email_setting() {
	return apply_filters( 'bfrf_admin_email_setting', get_option( '_admin_email_setting', '' ) );
}

/**
 * Retrieves admin email body from db.
 */
function bfrf_admin_message_setting() {
	return apply_filters( 'bfrf_admin_message_setting', get_option( '_admin_message_setting', 'New user registration on the site' ) );
}

/**
 * Retrieves admin email subject from db.
 */
function bfrf_admin_subject_setting() {
	return apply_filters( 'bfrf_admin_subject_setting', get_option( '_admin_subject_setting', 'New User Registration' ) );
}

/**
 * Retrieves brand id value from db.
 */
function bfrf_brand_id_dropdown() {
	return apply_filters( '_brand_id_dropdown', get_option( '_brand_id_dropdown', '' ) );
}

/**
 * Retrieves default country from db.
 */
function bfrf_country_setting() {
	return apply_filters( 'bfrf_country_setting', get_option( '_bfrf_country', __( 'us', 'bf_registration_form' ) ) );
}

/**
 * Retrieves error message from db.
 */
function bfrf_error_message_setting() {
	return apply_filters( 'bfrf_error_message_setting', get_option( '_error_message_setting', 'Due to some technical issues your email was not sent. Please try again later.' ) );
}

/**
 * Retrieves lda error message from db.
 */
function bfrf_lda_error_message_setting() {
	return apply_filters( 'bfrf_lda_error_message_setting', get_option( '_lda_error_message_setting', 'Sorry, you are not of legal drinking age.' ) );
}

/**
 * Retrieves segment id value from db.
 */
function bfrf_segment_id_setting() {
	return apply_filters( '_bfrf_segment_id', get_option( '_bfrf_segment_id', '4' ) );
}

/**
 * Retrieves user email body from db.
 */
function bfrf_sent_content_setting() {
	return apply_filters( 'bfrf_sent_content_setting', get_option( '_sent_content_setting', 'Thank you for your message, we will be in touch shortly.' ) );
}

/**
 * Retrieves sent heading from db.
 */
function bfrf_sent_heading_setting() {
	return apply_filters( 'bfrf_sent_heading_setting', get_option( '_sent_heading_setting', 'Email Sent' ) );
}

/**
 * Retrieves source code value from db.
 */
function bfrf_source_code_setting() {
	return apply_filters( '_bfrf_source_code', get_option( '_bfrf_source_code', '' ) );
}

/**
 * Retrieves user email subject from db.
 */
function bfrf_user_email_disable_setting() {
	return apply_filters( 'bfrf_user_email_diable', get_option( '_bfrf_user_email_disable', 'disabled' ) );
}

/**
 * Retrieves user email subject from db.
 */
function bfrf_user_email_from_setting() {
	return apply_filters( 'bfrf_user_email_from_setting', get_option( '_bfrf_user_email_from_setting', '' ) );
}

/**
 * Retrieves user email body from db.
 */
function bfrf_user_message_setting() {
	return apply_filters( 'bfrf_user_message_setting', get_option( '_bfrf_user_message_setting', 'Your registration details' ) );
}

/**
 * Retrieves user email subject from db.
 */
function bfrf_user_subject_setting() {
	return apply_filters( 'bfrf_user_subject_setting', get_option( '_bfrf_user_subject_setting', 'You have registered' ) );
}

/**
 * Retrieves recaptcha secret key from db.
 */
function bfrf_g_secret_key_setting() {
	return apply_filters( '_bfrf_g_secret_key', get_option( '_bfrf_g_secret_key', '' ) );
}

/**
 * Retrieves production URL value from db.
 */

function bfrf_prod_url_setting() {
	return apply_filters( '_bfrf_prod_url', get_option( '_bfrf_prod_url', '' ) );
}


/**
 * Retrieves brand id value from db.
 */
/*
function bfrf_brand_id_setting() {
	return apply_filters( '_bfrf_brand_id', get_option( '_bfrf_brand_id', '1' ) );
}
*/

function bfrf_print_promotion_checkbox_element($vars) {
    ?>
	<div class="bfrf-field-input">
		<label><input id="<?php echo $vars['q_id']; ?>" name="<?php echo $vars['q_id']; ?>" type="checkbox"  <?php echo $vars['i_required']; ?> value="<?php echo $vars['checked_value']; ?>" data-not-checked="<?php echo $vars['not_checked_value']; ?>"><?php echo $vars['q']; echo $vars['q_required']; ?></label>
	</div>
	<?php
}

function bfrf_print_promotion_radio_fieldset($vars) {
    $selected = NULL;
    ?>
        <fieldset>
            <div class="bfrf-field-label">
            	<legend><?php echo $vars['q']; echo $vars['q_required']; ?></legend>
            </div>
            <div class="bfrf-field-input bfrf-field-input-radio-fields-outer">
                <div class="bfrf-field-input-radio-fields">
                	<?php
						if (!empty($vars['selected'])) {
							$selected = $vars['selected'];
						}
						$r = 0;
						foreach($vars['options'] as $k=>$v) {
							$s = !empty($selected) && $v == $selected ? ' checked="checked"' : '';
							?>
							<div class="bfrf-radio-field">
								<label><input name="<?php echo $vars['q_id']; ?>" value="<?php echo $k; ?>" type="radio" <?php echo $s; ?>>&nbsp;&nbsp;<?php echo $k; ?></label>
							</div>
						<?php
							$r = $r + 1;
						}
                	?>
                </div>
            </div>
        </fieldset>
<?php }

function bfrf_print_promotion_select_element($vars) {
    $selected = NULL;
	?>
    <div class="bfrf-field-label">
    	<label for="<?php echo $vars['q_id'];?>"><?php echo $vars['q']; echo $vars['q_required']; ?></label>
	</div>
	<div class="bfrf-field-input">
		<select id="<?php echo $vars['q_id']; ?>"  name="<?php echo $vars['q_id']; ?>" <?php echo $vars['i_required']; ?>>
			<?php
				$blank = sprintf('<option value="">-- select an option --</option>');
				$options_str = "";
				if (!empty($vars['selected'])) {
					$selected = $vars['selected'];
				}
				foreach($vars['options'] as $k=>$v) {
					if ($v === "") {
						$blank = "";
					}
					$s = !empty($selected) && $v == $selected ? ' selected="selected"' : '';
					$options_str = $options_str.sprintf('<option value="%s"%s>%s</option>', $v, $s, $k );
				}
				echo $blank.$options_str;
			?>
		</select>
	</div>
	<?php
}

function bfrf_print_promotion_texty_element($vars) {
    ?>
    <div class="bfrf-field-label">
    	<label for="<?php echo $vars['q_id'];?>"><?php echo $vars['q']; echo $vars['q_required']; ?></label>
	</div>
	<?php
	if (in_array($vars['t'], array('text', 'number', 'email', 'tel'))){ ?>
        <div class="bfrf-field-input"><input id="<?php echo $vars['q_id']; ?>" name="<?php echo $vars['q_id']; ?>" type="<?php echo $vars['t']; ?>"  <?php echo $vars['placeholder']; echo $vars['i_required']; ?>></div>
	<?php } else if ($vars['t'] === 'textarea') { ?>
		<div class="bfrf-field-input bfrf-field-textarea"><textarea id="<?php echo $vars['q_id']; ?>" name="<?php echo $vars['q_id']; ?>"  <?php echo $vars['placeholder']; echo $vars['i_required']; ?>></textarea></div>
	<?php }
}

function bfrf_get_promotion_variable_array($p, $i) {
	$arr = array();
    $arr['q'] = $p['question'];
    $arr['checked_value'] = !empty($p['checked_value']) ? $p['checked_value'] : 'Y';
    $arr['not_checked_value'] = !empty($p['not_checked_value']) ? $p['not_checked_value'] : 'N';
    $arr['q_required'] = !empty($p['required']) && $p['required'] === true ? ' <span class="bf-star">*</span>' : '';
    $arr['i_required'] = !empty($p['required']) && $p['required'] === true ? 'required="required" aria-required="true"' : '';
    $arr['q_id'] = sprintf("bfrf-pq-%s", $i);
	$arr['t'] = $p['type'];
	$arr['placeholder'] = !empty($p['placeholder']) ? ' placeholder="'.$p['placeholder'].'"' : '';

	if (in_array($p['type'], array('select', 'radio'))) {
		$arr['options'] = bfrf_get_associative_array($p['options']);
		if (!empty($p['selected'])) {
			$arr['selected'] = $p['selected'];
		}
	}
	return $arr;
}

function bfrf_print_promotion_responses($promotion_questions){
    $i = 0;
    foreach($promotion_questions as $p) {
        if (!bfrf_promotion_question_sanitation_check($p)) {
            continue;
        }
        $vars = bfrf_get_promotion_variable_array($p, $i);
        ?>
        <div class="bfrf-field-container bfrf-promotion-response" data-type="<?php echo $vars['t']; ?>" data-question="<?php echo $vars['q']; ?>" data-target="<?php echo $vars['q_id']; ?>">
        	<?php
        	if ($p['type'] === 'radio') {
				bfrf_print_promotion_radio_fieldset($vars);
    		} else if ($p['type'] === 'select') {
				bfrf_print_promotion_select_element($vars);
    		} else if ($p['type'] === 'checkbox') {
				bfrf_print_promotion_checkbox_element($vars);
    		} else {
		        bfrf_print_promotion_texty_element($vars);
        	} ?>
        </div><?php
        $i = $i + 1;
    }
}

function bfrf_get_associative_array($arr) {
	if (bfrf_isAssoc($arr)) {
		return $arr;
	}
	$options = array();
	foreach($arr as $opt) {
		$options[$opt] = $opt;
	}
	return $options;
}

function bfrf_isAssoc(array $arr) {
	//http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

function bfrf_promotion_question_sanitation_check($p) {
    if (empty($p['question'] || empty($p['type']))) {
        return false;
    }
    if ( in_array($p['type'], ['select', 'radio']) && empty($p['options'])) {
        return false;
    }
    return true;
}