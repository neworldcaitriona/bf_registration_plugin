=== B-F Registration Form ===
Contributors: Norberto, Jeff, Josh

B-F Registration form with web-to-case integration.
Use shortcode: [bf-registration-form]

== Description ==

B-F Registration Form

== Installation ==


== Frequently Asked Questions ==
====Promotion Responses====
1. At the top of bfrf_form.php, add the variable $promotion_questions and define it as an array.
$promotion_questions = array()

2. Add objects to the array like this:
$promotion_questions[]= array(
    'question'=>'Question #1',
    'type' => 'text',
    'required' => true,
    'placeholder' => 'Answer me'
);

Valid keys are:
'question' => '' //string, required, the question to ask the user
'type' => '' //string, required, the type of field for the input. text, number, email, tel, texarea, select, radio, checkbox are valid options.
'required' => false //boolean, defaults to false, whether the field should be required
'placeholder' => '' //string, defaults to empty, placeholder for "texty" type fields
'default' => '' //string or number, defaults to empty, if a select or radio input should have a default value
'checked_value' => 'Y' //string, value to send to database is element is checked, defaults to 'Y'
'checked_value' => 'N' //string, value to send to database is element is not checked, defaults to 'N'
'options' => array() //array or associative array of options for a select or radio element.
	If a regular array is provided, the value and display text will be the value at that index in the array. array('display text and value')
	If an associative array is provided, the display text is the key, and the value is the value array('display text' => 'value')

== Testing ==

== Troubleshooting ==

If Javascript is not loading correctly, check the wp_enqueue_script call in the enqueue_scripts function of bf_registration_plugin.php. The script is enqueued with a JQuery dependency, but some Wordpress installations load JQuery with a different handle name, or with different handle punctuation.

== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision

= 1.0 =
- Web-to-case integration
